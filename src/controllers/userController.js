const User = require('../models/User');
const bcrypt = require('bcryptjs');

exports.getProfileForUser = async (req, res) => {
  try {
    const {userId} = req.user;
    const user = await User.findById({_id: userId}).select('-__v -password');

    res.status(200).send({user});
  } catch (error) {
    res.status(400).send({message: error.message});
  }
};

exports.deleteProfileForUser = async (req, res) => {
  try {
    const {userId} = req.user;
    await User.findByIdAndRemove({_id: userId});

    res.status(200).send({message: 'Profile deleted successfully'});
  } catch (error) {
    res.status(400).send({message: error.message});
  }
};

exports.changePasswordForUser = async (req, res) => {
  try {
    const {userId} = req.user;
    const {newPassword, oldPassword} = req.body;
    const user = await User.findById({_id: userId});

    if (!(await bcrypt.compare(oldPassword, user.password))) {
      return res.status(400).send(
          {message: 'Please enter correct password!'},
      );
    }

    const salt = await bcrypt.genSalt(10);
    user.password = await bcrypt.hash(newPassword, salt);

    await user.save();

    res.status(200).send({message: 'Password changed successfully'});
  } catch (error) {
    res.status(400).send({message: error.message});
  }
};
