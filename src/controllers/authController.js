const User = require('../models/User');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const {InvalidRequestError} = require('../utils/errors');
const asyncWrapper = require('../utils/asyncWrapper');

exports.postRegister = asyncWrapper(async (req, res, next) => {
  const {email, password, role} = req.body;
  const userExists = await User.findOne({email});

  if (userExists) {
    throw new InvalidRequestError('User already exists');
  }

  const salt = await bcrypt.genSalt(10);
  const hashedPassword = await bcrypt.hash(password, salt);

  const user = new User({
    email,
    password: hashedPassword,
    role,
  });

  await user.save();

  res.status(200).json({message: 'Profile created successfully'});
});

exports.postLogin = asyncWrapper(async (req, res) => {
  const {email, password} = req.body;

  const user = await User.findOne({email});
  if (!user) {
    throw new InvalidRequestError('User not found!');
  }

  const validPass = await bcrypt.compare(password, user.password);
  if (!validPass) throw new InvalidRequestError('Invalid password!');


  const token = jwt.sign({
    _id: user._id,
    email: user.email,
    role: user.role,
  }, 'secret');

  res.status(200).json({jwt_token: token});
});


exports.forgotPassword = asyncWrapper(async (req, res) => {
  const {email} = req.body;

  const user = await User.findOne({email});
  if (!user) {
    throw new InvalidRequestError('User not found!');
  }

  res.status(200).json({message: 'New password sent to your email address'});
});
