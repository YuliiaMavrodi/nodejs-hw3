const Truck = require('../models/Truck');
const Load = require('../models/Load');
const {LOAD_STATE} = require('../utils/loadConstants');
const {InvalidRequestError} = require('../utils/errors');
const asyncWrapper = require('../utils/asyncWrapper');

exports.getLoadsForUser = asyncWrapper(async (req, res) => {
  const {userId, role} = req.user;
  if (role === 'DRIVER') {
    const loads = await Load.find({
      $or: [{status: 'POSTED'}, {status: 'SHIPPED'}],
      assigned_to: userId,
    }).select('-__v');

    return res.status(200).json({loads});
  }
  if (role === 'SHIPPER') {
    const loads = await Load.find({created_by: userId}).select('-__v');
    return res.status(200).json({loads});
  }
});

exports.postLoadForUser = asyncWrapper(async (req, res) => {
  const loadInfo = {...req.body};
  const {userId} = req.user;
  const load = new Load({
    created_by: userId,
    ...loadInfo,
  });

  await load.save();

  res.status(200).json({message: 'Load created successfully'});
});

exports.getActiveLoadForUser = asyncWrapper(async (req, res) => {
  const {userId} = req.user;

  const load = await Load.findOne({assigned_to: userId});

  if (!load) {
    throw new InvalidRequestError('You do not have active load yet');
  }

  res.status(200).json({load});
});


exports.patchIterateToNextLoadState = asyncWrapper(async (req, res) => {
  const {userId} = req.user;

  const load = await Load.findOne({
    assigned_to: userId,
    status: {$ne: 'SHIPPED'},
  });

  if (!load) {
    throw new InvalidRequestError('You do not have active load yet');
  }

  if (load.status === 'SHIPPED') {
    throw new InvalidRequestError(
        'Load is already shipped, you can not change status',
    );
  }

  let newState;

  const loadStateMessage = `Load state changed to '${newState}'`;

  if (load.state === null) {
    newState = LOAD_STATE[0];
  } else {
    const currStateIndex = LOAD_STATE.findIndex((el) => el === load.state);
    newState = LOAD_STATE[currStateIndex + 1];
    if (newState === LOAD_STATE[3]) {
      load.status = 'SHIPPED';
      const truck = await Truck.findOne({assigned_to: userId});
      truck.status = 'IS';
      await truck.save();
    }
  }
  load.logs.push({message: loadStateMessage});
  load.state = newState;
  await load.save();

  res.status(200).json({message: loadStateMessage});
});

exports.getLoadForUserById = asyncWrapper(async (req, res) => {
  const {userId, role} = req.user;
  const loadId = req.params.id;

  const load = await Load.findOne({_id: loadId}).select('-__v');

  if (role === 'SHIPPER' && load.created_by !== userId) {
    throw new InvalidRequestError('You do not have access to this load');
  }
  if (role === 'DRIVER' && load.assigned_to !== userId) {
    throw new InvalidRequestError('You do not have access to this load');
  }

  res.status(200).json({load});
});

exports.updateLoadForUser = asyncWrapper(async (req, res) => {
  const loadId = req.params.id;
  const {userId} = req.user;

  const load = await Load.findOne({_id: loadId, created_by: userId});

  if (!load) {
    throw new InvalidRequestError('Load not found');
  }
  if (load.status !== 'NEW') {
    throw new InvalidRequestError(
        'You can not update load while it is in work',
    );
  }

  const updatedFields = {...req.body};
  Object.assign(load, updatedFields);
  await load.save();

  res.status(200).json({message: 'Load details changed successfully'});
});

exports.deleteLoadForUser = asyncWrapper(async (req, res) => {
  const loadId = req.params.id;
  const {userId} = req.user;

  const load = await Load.findOne({_id: loadId, created_by: userId});

  if (!load) {
    throw new InvalidRequestError('Load not found');
  }

  if (load.status !== 'NEW') {
    throw new InvalidRequestError(
        'You can not delete load while it is in work',
    );
  }
  await load.remove();

  res.status(200).json({message: 'Load deleted successfully'});
});

exports.postLoadForUserById = asyncWrapper(async (req, res) => {
  const loadId = req.params.id;
  const {userId} = req.user;

  const load = await Load.findOne({_id: loadId, created_by: userId});

  if (!load) {
    throw new InvalidRequestError('Load not found');
  }
  load.status = 'POSTED';
  await load.save();

  const loadDimensions = load.dimensions;

  const truck = await Truck.findOne({
    'assigned_to': {$ne: null},
    'dimensions.width': {$gte: loadDimensions.width},
    'dimensions.length': {$gte: loadDimensions.length},
    'dimensions.capacity': {$gte: load.payload},
    'status': 'IS',
  });

  if (!truck) {
    load.status = 'NEW';
    await load.save();
    throw new InvalidRequestError('Driver not found');
  } else {
    load.status = 'ASSIGNED';
    load.state = LOAD_STATE[0];
    load.logs.push({
      message: `Load assigned to driver with id ${truck.assigned_to}`,
    });
    truck.status = 'OL';
    load.assigned_to = truck.assigned_to;

    await load.save();
    await truck.save();
    return res.status(200).json({
      message: 'Load posted successfully',
      driver_found: true,
    });
  }
});

exports.getLoadShippingInfoForUser = asyncWrapper(async (req, res) => {
  const {userId} = req.user;
  const loadId = req.params.id;

  const load = await Load.findOne({_id: loadId, created_by: userId})
      .select('-__v');
  const truck = await Truck.findOne({assigned_to: load.assigned_to})
      .select('-__v -dimensions');

  if (!load) {
    throw new InvalidRequestError('Load not found');
  }
  if (!truck) {
    throw new InvalidRequestError('Truck not found');
  }

  res.status(200).json({load, truck});
});
