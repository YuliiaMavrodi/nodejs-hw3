const jwt = require('jsonwebtoken');
const {InvalidRequestError} = require('../utils/errors');

const authMiddleware = (req, res, next) => {
  const {authorization} = req.headers;

  if (!authorization) {
    throw new InvalidRequestError('Please provide \'authorization\' header!');
  }

  const token = authorization.split(' ')[1];

  if (!token) {
    throw new InvalidRequestError('Please provide correct token!');
  }

  try {
    const verify = jwt.verify(token, 'secret');
    req.user = {
      userId: verify._id,
      email: verify.email,
      role: verify.role,
    };
    next();
  } catch (error) {
    res.status(400).send({message: error.message});
  }
};

module.exports = authMiddleware;
