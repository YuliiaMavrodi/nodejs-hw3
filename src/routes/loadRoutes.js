const express = require('express');

const loadController = require('../controllers/loadController');

const {shipperMiddleware} = require('../middlewares/shipperMiddleware');
const {driverMiddleware} = require('../middlewares/driverMiddleware');

const router = new express.Router();

router.get('/active', driverMiddleware,
    loadController.getActiveLoadForUser);

router.patch('/active/state', driverMiddleware,
    loadController.patchIterateToNextLoadState);

router.get('/:id', loadController.getLoadForUserById);

router.put('/:id', shipperMiddleware,
    loadController.updateLoadForUser);

router.delete('/:id', shipperMiddleware,
    loadController.deleteLoadForUser);

router.post('/:id/post', shipperMiddleware,
    loadController.postLoadForUserById);

router.get('/:id/shipping_info', shipperMiddleware,
    loadController.getLoadShippingInfoForUser);

router.post('/', shipperMiddleware,
    loadController.postLoadForUser);

router.get('/', loadController.getLoadsForUser);


module.exports = router;
