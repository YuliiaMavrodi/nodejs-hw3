const express = require('express');

const authController = require('../controllers/authController');
const registrationValidation = require('../middlewares/registrationValidation');

const router = new express.Router();

router.use(registrationValidation);

router.post('/register', authController.postRegister);
router.post('/login', authController.postLogin);
router.post('/forgot_password', authController.forgotPassword);

module.exports = router;
