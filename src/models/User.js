const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
  email: {
    type: String,
    required: true,
    max: 255,
    min: 6,
  },
  password: {
    type: String,
    required: true,
    max: 255,
    min: 6,
  },
  role: {
    type: String,
    enum: ['DRIVER', 'SHIPPER'],
    required: true,
  },
  createdDate: {
    type: Date,
    default: Date.now,
  },
});


module.exports = mongoose.model('User', userSchema);
